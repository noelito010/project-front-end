**Realizados**
Modularización (Routing)                        (15 puntos)
  *secure
  *contacts
Lazy loading modules                            (10 puntos)
Ciclo de vida de un componente                  (10 puntos)
  *ngOnInit
  *ngOnDestroy -> desuscripcion
Componentes Stateful / Stateless                (20 puntos)
  *Stateful -> contact-list
  *Stateless -> contact-item
Servicios                                       (5 puntos)
Interceptores                                   (5 puntos)
LocalStorage                                    (5 puntos)
Formularios reactivos                           (10 puntos)
  *login
  *register-company
Carga por demanda (paginación)                  (5 puntos)
  *contact-list
POST. Create Company (account)                  (5 puntos)
POST. Authentication                            (5 puntos)
POST. Search                                    (10 puntos) 
  *contact-list
