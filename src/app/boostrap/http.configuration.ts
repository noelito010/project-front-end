import {environment} from '../../environments/environment';

export abstract class HttpConfiguration {
  private readonly host = environment.api.host;

  public getApiAddress(): string {
    return this.host;
  }
}
