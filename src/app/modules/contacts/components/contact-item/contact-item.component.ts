import {Component, Input, OnInit} from '@angular/core';

import {ContactItem} from '../../model/contact-item';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {
  @Input() contactItem: ContactItem;

  constructor() {
  }

  ngOnInit() {
  }

}
