import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsCreateContactComponent } from './contacts-create-contact.component';

describe('ContactsCreateContactComponent', () => {
  let component: ContactsCreateContactComponent;
  let fixture: ComponentFixture<ContactsCreateContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsCreateContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsCreateContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
