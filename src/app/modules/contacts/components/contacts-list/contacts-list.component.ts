import {Component, OnInit} from '@angular/core';
import {ContactsListService} from '../../services/contacts-list.service';
import {SearchItem} from '../../model/search-item';
import {Subscription} from 'rxjs';
import {ContactItem} from '../../model/contact-item';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss'],
  providers: [
    ContactsListService
  ]
})
export class ContactsListComponent implements OnInit {
  public searchCriteria: SearchItem;
  public contactList: ContactItem [];

  private _page: number;
  private limit: number;

  private _contactsListSubscription: Subscription;

  constructor(private _contactsListService: ContactsListService) {
    this.contactList = [];
    this.searchCriteria = {
      searchCriteria: ''
    };

    this._contactsListSubscription = new Subscription();
    this.limit = 10;
    this._page = 0;
  }

  ngOnInit() {
    this._initialize();
  }

  public searchList(): void {
    this._page = 0;

    this._loadContacts(this.limit, this._page);
  }

  public onScroll(): void {
    this._loadContacts(this.limit, this._page);
  }

  private _initialize(): void {
    this._loadContacts(this.limit, this._page);
  }

  private _loadContacts(limit: number, page: number): void {
    this._contactsListService.contactsList(this.searchCriteria, limit, page)
      .toPromise().then((contacts: ContactItem []) => {
      const hasContacts: boolean = this.contactList.length > 0;
      this.contactList = hasContacts ? this.contactList.concat(contacts) : contacts;
      this._page++;
    });
  }

}
