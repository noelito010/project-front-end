import {Routes} from '@angular/router';

import {ContactsCreateContactComponent} from '../components/contacts-create-contact/contacts-create-contact.component';
import {ContactsListComponent} from '../components/contacts-list/contacts-list.component';
import {ContactsMainComponent} from '../components/contacts-main/contacts-main.component';

export const CONTACTS_ROUTES: Routes = [
  {
    path: '',
    component: ContactsMainComponent,
    children: [
      {
        path: 'contacts-list',
        component: ContactsListComponent
      },
      {
        path: 'contact-create',
        component: ContactsCreateContactComponent
      },
      {
        path: '',
        redirectTo: '/contacts/contacts-list',
        pathMatch: 'full'
      }
    ]
  }
];
