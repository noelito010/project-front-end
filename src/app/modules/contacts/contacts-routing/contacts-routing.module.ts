import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CONTACTS_ROUTES} from './contacts-routes';

@NgModule({
  imports: [
    RouterModule.forChild(CONTACTS_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class ContactsRoutingModule {
}
