import {NgModule} from '@angular/core';

import {SharedModule} from '../shared/shared.module';
import {ContactsRoutingModule} from './contacts-routing/contacts-routing.module';

import {ContactsCreateContactComponent} from './components/contacts-create-contact/contacts-create-contact.component';
import {ContactsListComponent} from './components/contacts-list/contacts-list.component';
import {ContactsMainComponent} from './components/contacts-main/contacts-main.component';
import {ContactItemComponent} from './components/contact-item/contact-item.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    SharedModule,
    ContactsRoutingModule
  ],
  declarations: [
    ContactsMainComponent,
    ContactsCreateContactComponent,
    ContactsListComponent,
    ContactItemComponent
  ]
})
export class ContactsModule {
}
