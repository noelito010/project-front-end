export interface ContactItem {
  information: string;
  withoutAccountOwner: boolean;
}
