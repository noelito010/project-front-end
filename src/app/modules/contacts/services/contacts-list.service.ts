import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpService} from '../../../boostrap/http.service';

import {SearchItem} from '../model/search-item';

@Injectable()
export class ContactsListService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/contact/secure/contacts/search?';
  }

  public contactsList(request: SearchItem, limit: number, page: number): Observable<any> {
    console.log(request);
    return this.httpClient().post(`${this.getUrl()}limit=${limit}&page=${page}`, request);
  }

  protected injector(): Injector {
    return this._injector;
  }

}
