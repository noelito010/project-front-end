import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';

import {SecureAccountService} from '../../services/secure-account.service';
import {unsubscribe} from '../../../shared/utils/unsubscribe-subscription';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../shared/services/authentication.service';

@Component({
  selector: 'app-secure-login',
  templateUrl: './secure-login.component.html',

  providers: [SecureAccountService]
})
export class SecureLoginComponent implements OnInit, OnDestroy {
  public formLogin: FormGroup;

  private _loginSubscription: Subscription;

  constructor(private _accountService: SecureAccountService,
              private authService: AuthenticationService,
              private _fb: FormBuilder,
              private _router: Router) {
    this._loginSubscription = new Subscription();
  }

  ngOnInit() {
    this._initialize();
  }

  ngOnDestroy() {
    unsubscribe(this._loginSubscription);
  }

  private _initialize(): void {
    this._initForm();
  }

  private _initForm() {
    this.formLogin = this._fb.group({
      confirmPassword: [null],
      email: [null],
      name: [null],
      password: [null],
    });
  }

  public login(): void {
    this._loginSubscription = this._accountService.getAccount(this.formLogin.value)
      .subscribe((response: { token: string }) => {
          this.authService.setToken(response.token);

          this._router.navigate(['contacts']);
        },
        () => {
          console.log('User or password is not valid.');
        });
  }

}
