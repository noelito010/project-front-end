import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureRegisterComponent } from './secure-register.component';

describe('SecureRegisterComponent', () => {
  let component: SecureRegisterComponent;
  let fixture: ComponentFixture<SecureRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
