import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SecureCompanyRegisterService} from '../../services/secure-company-register.service';
import {Subscription} from 'rxjs';
import {Company} from '../../model/company';
import {Router} from '@angular/router';
import {unsubscribe} from '../../../shared/utils/unsubscribe-subscription';

@Component({
  selector: 'app-secure-register',
  templateUrl: './secure-register.component.html',
  styleUrls: ['./secure-register.component.scss'],
  providers: [
    SecureCompanyRegisterService
  ]
})
export class SecureRegisterComponent implements OnInit, OnDestroy {
  public formRegister: FormGroup;

  private _companyRegisterSubscription: Subscription;

  constructor(private _secureCompanyRegisterService: SecureCompanyRegisterService,
              private _fb: FormBuilder,
              private _router: Router) {
    this._companyRegisterSubscription = new Subscription();
  }

  ngOnInit() {
    this._initialize();
  }

  ngOnDestroy() {
    unsubscribe(this._companyRegisterSubscription);
  }

  private _initialize(): void {
    this._initForm();
  }

  private _initForm() {
    this.formRegister = this._fb.group({
      confirmPassword: [null],
      email: [null],
      name: [null],
      password: [null],
    });
  }

  public createCompany(): void {
    this._companyRegisterSubscription = this._secureCompanyRegisterService.registerCompany(this.formRegister.value)
      .subscribe((company: Company) => {
        this._router.navigate([`/secure/login`]);
      });
  }
}
