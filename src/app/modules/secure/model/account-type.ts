export enum AccountType {
  PERSON = 'PERSON',
  COMPANY = 'COMPANY'
}
