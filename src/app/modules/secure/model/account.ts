import {AccountState} from './account-state';
import {AccountType} from './account-type';

export interface Account {
  accountId: number;
  active: boolean;
  email: string;
  id: number;
  accountType: AccountType;
  state: AccountState;
}
