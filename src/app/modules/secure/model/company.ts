import {AccountType} from './account-type';

export interface Company {
  active: boolean;
  createdDate: string;
  email: string;
  id: number;
  name: string;
  password: string;
  userType: AccountType;
}
