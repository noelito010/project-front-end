import {Routes} from '@angular/router';
import {SecureLoginComponent} from '../components/secure-login/secure-login.component';
import {SecureMainComponent} from '../components/secure-main/secure-main.component';
import {SecureRegisterComponent} from '../components/secure-register/secure-register.component';

export const SECURE_ROUTES: Routes = [
  {
    path: '',
    component: SecureMainComponent,
    children: [
      {
        path: 'login',
        component: SecureLoginComponent
      },
      {
        path: 'sign-up',
        component: SecureRegisterComponent
      },
      {
        path: '',
        redirectTo: '/secure/login',
        pathMatch: 'full'
      }
    ]
  }
];
