import {NgModule} from '@angular/core';

import {SecureRoutingModule} from './secure-routing/secure-routing.module';
import {SharedModule} from '../shared/shared.module';

import {SecureLoginComponent} from './components/secure-login/secure-login.component';
import {SecureMainComponent} from './components/secure-main/secure-main.component';
import {SecureRegisterComponent} from './components/secure-register/secure-register.component';

@NgModule({
  imports: [
    SharedModule,
    SecureRoutingModule
  ],
  declarations: [
    SecureLoginComponent,
    SecureMainComponent,
    SecureRegisterComponent
  ]
})
export class SecureModule {
}
