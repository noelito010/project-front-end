import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpService} from '../../../boostrap/http.service';

import {CompanyFormRegister} from '../model/company-form-register';
import {AccountCompanyForm} from '../model/account-company-form';
import {HttpResponse} from '@angular/common/http';

@Injectable()
export class SecureAccountService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/security/auth';
  }

  public getAccount(request: AccountCompanyForm): Observable<object> {
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }

}
