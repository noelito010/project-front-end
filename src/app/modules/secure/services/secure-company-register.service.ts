import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpService} from '../../../boostrap/http.service';

import {CompanyFormRegister} from '../model/company-form-register';

@Injectable()
export class SecureCompanyRegisterService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/users/public/companies';
  }

  public registerCompany(request: CompanyFormRegister): Observable<any> {
    console.log(request);
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }

}
