import {Routes} from '@angular/router';

export const APP_ROUTES: Routes = [
  {
    path: 'secure',
    loadChildren: '../modules/secure/secure.module#SecureModule'
  },
  {
    path: 'contacts',
    loadChildren: '../modules/contacts/contacts.module#ContactsModule'
  },
  {
    path: '',
    redirectTo: '/secure/login',
    pathMatch: 'full'
  }
];
